# What is this

Basic real time messaging web-app built with socket.io

## Requirements
```
npm
```
## How to run

after clone the repository,

Go to terminal
```
$ npm install
```
```
$ node start
```
Go to your web browser,

```
http://localhost:3000
```

### Thanks to

* [Traversy Media ](https://youtu.be/8Y6mWhcdSUM) - Socket.io Chat App Using Websockets

